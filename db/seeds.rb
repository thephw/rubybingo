# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
gay_atlanta_game = Game.where(title: "Gay Atlanta", is_template: true).first
if gay_atlanta_game.nil?
  gay_atlanta_game = Game.create title: "Gay Atlanta", is_template: true
end
gay_atlanta_words = [
  'Mellow Mushroom',
  'Blake\'s',
  'Burkhart\'s',
  'Oscars',
  'Felix\'s',
  'The Hideaway',
  'The Eagle',
  'Red Chair',
  'Backstreet',
  'MSR',
  'Mary\'s',
  'Sister Louisa\'s Church',
  'Midtown Art Cinema',
  '3 Legged Cowboy',
  'Brushstrokes',
  'FLEX',
  'Eros',
  'Cheshire Motor Inn',
  'Piedmont Park',
  'Ansley Mall',
  'Amsterdam',
  'Woofs',
  'Jungle',
  'BJ Roosters',
  'The Heretic',
  'Boy Next Door',
  'Joe\'s on Juniper',
  'Bulldogs',
  'Model-T',
  'The Cockpit',
  'David Magazine',
  'Fenuxe Magazine',
  'GA Voice',
  'Creative Loafing',
  'The Armorettes',
  'The Sisters',
  'Wild Cherry Sucret',
  'Swinging Richards',
  'Al\'Meria RichMan',
  'Ursula Polari',
  'Ally Yankadic',
  'B\'Yonda Cloud',
  'Sofonda Cox',
  'Edie Cheezburger',
  'Vivian Valium',
  'Princess Alberta',
  'Trashetta Galore',
  'Amber Divine',
  'Sugarbaker',
  'BrentStar',
  'Jaye Lish',
  'Ginger Vitis',
  'Lucy Bowels',
  'Alexandria Martin',
  'Knomie Moore',
  'Gunza Blazin',
  'Prissy Go-Lightly',
  'Sissy SweetTea',
  'Ginger Bred Man',
  'Nurse Holly',
  'Rick Westbrook',
  'Laura Gentle',
  'Marcella Grrleen Redd',
  'River Song-Redd',
  'Rapture Divine Cox',
  'Natalie\'s Boobs',
  'Seth',
  'Lily White',
  'Metro',
  'Gilbert\'s',
  'Las Margaritas',
  'Bliss',
  'Dragnique',
  'Wet Bar',
  'Mixx'
]
if gay_atlanta_game.all_words != gay_atlanta_words.count
  gay_atlanta_game.all_words = gay_atlanta_words
  gay_atlanta_game.save
end